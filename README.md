# repolocheck

```txt
usage: repolocheck [-h] [-i FILE] [-c FILE]

Display a list of distros by project availability.

optional arguments:
  -h, --help            show this help message and exit
  -i FILE, --input-projects FILE
                        input file with a list of projects. If it's your first run, you
                        should provide this.
  -c FILE, --data-cache FILE
                        repology data json. defaults to '$PWD/repologydump.json'. It
                        will be created if not present.
```

Input file (`input-projects.txt`) must contain project names (you'll have to look them up on [repology](https://repology.org/)):

```txt
vscode
fish
zsh
...
```

Run `repolocheck -i input-projects.txt` to download projects' data. It will be saved to `repologydump.json` (or somewhere else if you specify the location with `-c` argument).

After processing the data, a long list of distros and packages will be shown. For example:

```txt
 ...

 manjaro_testing
        Has (58): vscode fish bleachbit fzf redshift syncthing xdo telegram-desktop pkgfile gimp neofetch firefox shfmt luajit fff nnn xdotool veracrypt xclip fonts:firacode bat keepassxc sxiv sxhkd vifm screenfetch wmctrl meld inkscape krita borgbackup trash-cli mpv quiterss picom thunderbird console-solarized jq youtube-dl moneymanagerex zsh imv cantata exa rofi sqlitebrowser bspwm xcape imagemagick shellcheck yay digikam chafa dmenu links bash-language-server appmenu-gtk-module qbittorrent
        Missing (8): mergerfs godot oomox msi-rgb ufetch vscodium aseprite steam

 manjaro_stable
        Has (58): vscode fish bleachbit fzf redshift syncthing xdo telegram-desktop pkgfile gimp neofetch firefox shfmt luajit fff nnn xdotool veracrypt xclip fonts:firacode bat keepassxc sxiv sxhkd vifm screenfetch wmctrl meld inkscape krita borgbackup trash-cli mpv quiterss picom thunderbird console-solarized jq youtube-dl moneymanagerex zsh imv cantata exa rofi sqlitebrowser bspwm xcape imagemagick shellcheck yay digikam chafa dmenu links bash-language-server appmenu-gtk-module qbittorrent
        Missing (8): mergerfs godot oomox msi-rgb ufetch vscodium aseprite steam

 aur
        Has (59): vscode fish bleachbit fzf godot oomox redshift syncthing xdo telegram-desktop pkgfile gimp neofetch firefox luajit fff mergerfs nnn xdotool veracrypt ufetch fonts:firacode bat keepassxc vscodium sxiv sxhkd vifm screenfetch meld inkscape krita borgbackup mpv quiterss picom thunderbird console-solarized jq youtube-dl moneymanagerex zsh imv cantata exa rofi msi-rgb bspwm sqlitebrowser xcape imagemagick shellcheck yay digikam links dmenu aseprite appmenu-gtk-module qbittorrent
        Missing (7): trash-cli xclip chafa shfmt bash-language-server steam wmctrl
```

To see it again, you can run `repolocheck -c repologydump.json`.

---

## Todo

- More output styles
- Cache updating
